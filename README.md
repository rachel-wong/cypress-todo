## 🧠Cypress Testing Basics

Using React Todo app forked from [Kingsley Chijioke](https://github.com/kinsomicrote/cypress-react-tutorial)

- automate UI manual clicking, network tests, parallelize testing

### Firing script

> npx cypress open

OR

Change `package.json` scripts to `"cypress": "cypress open" and then run 

> npm cypress run

### Notes on Commands

`.get(element)` - find/targets html element of a certain class or id (on DOM?)

`.find("classname")`- find the existence of an element with the particular classname

`first()` - return the first DOM thing from the parent `.get()`

`click()` - just that, a click. It can only click on one thing at a time though.

`should('have.something', value)` - sets out an assertion. What value something iterally should have.

`beforeEach(callback to do something)` - for each test inside the suite defined by the `describe`, do whatever that is defined in the `beforeEach`'s callback

`.focused()` - grab whatever that is in a focussed state

`.type(value)` - type in whatever the value in the argument

`first()` - returns the first element in the DOM from the parent definition i.e. `get()`

