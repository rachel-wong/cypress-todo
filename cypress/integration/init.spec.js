// Check to see whether cypress is working properly on the app
describe('Cypress base testing', () => {
  // Checks whether true = true 
  it('is Cypress working properly as a base', () => {
    expect(true).to.equal(true)
  })
  it('visits the app', () => {
    // cy.visit("http://localhost:3000")
    cy.visit('/') // after setting up the baseUrl in cypress.json
  })
})

// creates a separate a suite of tests
describe("another round of tests", () => {
  // Check to see whether we can visit the app at localhost:3000 on chrome browser
})