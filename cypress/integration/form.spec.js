// Check the input is in focus when app loads so users can start entering words immediately
describe("Form is working", () => {
  // before each test runs in this suite, open site in browser
  beforeEach(() => {
    cy.visit("/")
  })

  // after that, see that something is focussed AND it has a class of 'form-control'
  // .focused() => find an element of focus (has autofocus)
  it("it focuses the input", () => {
    cy.focused().should('have.class', 'form-control')
  })

  it("form accepts input", () => {
    const dummy = "Learn about cypress" //dummy text
    // does whatever that has the class .form-control, type in dummy text, the element should have the value showing up 
    // this mimicks that the form input works properly
    cy.get('.form-control').type(dummy).should('have.value', dummy)
  })
  // check that by default (when app runs), there is two task list items
  it("has two items by default", () => {
    cy.get('li').should('have.length', 2)
  })

  // check that when you type in a new todo, hit enter, there should be three li
  it("adds a to do list", () => {
    const dummytodo = "learn about cypress"
    // for the form-control element, enter in dummy text,  
    cy.get('.form-control').type(dummytodo).type('{enter}').get('li').should('have.length', 3)
  })

  it("all todos having a delete button", () => {
    cy.get("li").find(".btn-danger")
    console.log(cy.get("li"))
  })

  // empty the default todo list items should result in the 'No task!' text appearing 
  it("Deleting all default todo list items", () => {
    cy
      .get("li").first().find(".btn-danger").click() // first remove => one left
      .get("li").first().find('.btn-danger').click() // second remove => empty list
      .get('div.no-task').should('have.text', "No task!") // should get "no task!" txt
  })
})